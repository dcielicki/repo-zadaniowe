import java.util.stream.Stream;

public class Samochód extends Pojazd {

    private String brandName;

    public Samochód(String brandName) {
        super();
        this.brandName = brandName;
    }

    public String getBrandName() {
        return brandName;
    }

    @Override
    public void go() {
        if(isDriving()){
            System.out.println("Samochód już jedzie.");
        } else {
            System.out.println("Samochód rusza!");
            setDriving(true);
        }
    }

    @Override
    public void stop() {
        if(!isDriving()){
            System.out.println("Samochód już stoi.");
        } else {
            System.out.println("Samochód gwałtownie hamuje!");
            setDriving(false);
        }
    }

    @Override
    public void accelerate() {
        if(getSpeed() > 218){
            System.out.println("Nie mogę szybciej jechać!");
        } else {
            if(getSpeed() > 100){
                int counter = 0;
                for(int i = 0; i < getBrandName().length(); i++){
                    if(getBrandName().toLowerCase().charAt(i) == 'a'){
                        counter++;
                    }
                }
                setSpeed(getSpeed() + 2 + 2 * counter);
                System.out.println("Samochód przyspieszył o 2 km/h. Zasuwa teraz " + getSpeed() + " km/h.");
            } else{
                setSpeed(getSpeed() + 2);
                System.out.println("Samochód przyspieszył o 2 km/h. Zasuwa teraz " + getSpeed() + " km/h.");
            }
        }
    }

    @Override
    public void decelerate() {
        if(getSpeed() < 35){
            System.out.println("Chcesz cofać zbyt szybko!");
        } else {
            setSpeed(getSpeed() - 5);
            System.out.println("Samochód zwolnił o 5 km/h. Obecna prędkość to " + getSpeed() + " km/h.");
        }
    }

    @Override
    public void honk() {
        System.out.println("Honk, honk!");
    }
}
