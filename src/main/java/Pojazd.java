public abstract class Pojazd {

    private int speed;
    private boolean isDriving;

    public Pojazd() {
        this.speed = 0;
        this.isDriving = false;
    }

    public boolean isDriving() {
        return isDriving;
    }

    public void setDriving(boolean driving) {
        isDriving = driving;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public abstract void go();
    public abstract void stop();
    public abstract void accelerate();
    public abstract void decelerate();
    public abstract void honk();
}
